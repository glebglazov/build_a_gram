$:.unshift(File.expand_path('../lib', __FILE__))

require 'build_a_gram/version'

Gem::Specification.new do |s|
  s.name          = 'build_a_gram'
  s.version       = BuildAGram::Version
  s.licenses      = ['MIT']
  s.summary       = "Provides fast way of creating basic Telegram bots"
  s.authors       = ["Gleb Glazov"]
  s.email         = 'glazov.gleb@gmail.com'

  s.files         = `git ls-files`.split($/)
  s.executables   = s.files.grep(%r{^bin/}).map { |f| File.basename(f) }
  s.test_files    = s.files.grep(%r{^(test|spec|features)/})
  s.require_paths = ["lib"]

  s.add_dependency 'redis'
  s.add_dependency 'telegram-bot-ruby'
  s.add_dependency 'dry-inflector'

  s.add_development_dependency 'pry'
  s.add_development_dependency 'minitest'
end
