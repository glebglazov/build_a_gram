require 'dry/inflector'

module BuildAGram
  module Adapters
    def self.build(type: :redis, **options)
      require "build_a_gram/adapters/#{type}"

      inflector = Dry::Inflector.new

      const_get(inflector.camelize(type)).new(options)
    end
  end
end
