require 'build_a_gram/strategy'

module BuildAGram
  module Workers
    module MessageHandlers
      class Processor
        def self.start(**options)
          adapter_options = options.fetch(:adapter, {})
          logger          = options.fetch(:logger, BuildAGram.logger)

          adapter = Adapters.build(adapter_options.merge(logger: logger))

          new(**options.merge(adapter: adapter)).start
        end

        attr_reader :adapter, :token, :strategy, :logger

        def initialize(adapter:,
                       token:,
                       strategy_builder:,
                       logger: BuildAGram.logger)
          @adapter  = adapter
          @token    = token
          @strategy = strategy_builder.(api)
          @logger   = logger
        end

        def start
          logger.info('Starting to process messages...')

          loop do
            adapter.pop.tap do |msg|
              logger.debug(msg)
              strategy.call(msg)
            end
          end
        end

        def api
          @api ||= Telegram::Bot::Api.new(token)
        end
      end
    end
  end
end
