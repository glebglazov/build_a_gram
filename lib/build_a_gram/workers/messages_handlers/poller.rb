require 'build_a_gram/adapters'

module BuildAGram
  module Workers
    module MessageHandlers
      class Poller
        def self.start(**options)
          adapter_options = options.fetch(:adapter, {})
          logger          = options.fetch(:logger, BuildAGram.logger)

          adapter = Adapters.build(adapter_options.merge(logger: logger))

          new(options.merge(adapter: adapter)).start
        end

        attr_reader :adapter, :token, :logger

        def initialize(adapter:,
                       token:,
                       logger: BuildAGram.logger)
          @adapter   = adapter
          @logger    = logger
          @token     = token
        end

        def start
          Telegram::Bot::Client.run(token, logger: logger) do |bot|
            bot.listen do |message|
              adapter.push(message)
            end
          end
        rescue Errno::ECONNRESET => e
          logger.error(e.message, e.backtrace)
          retry
        end
      end
    end
  end
end
