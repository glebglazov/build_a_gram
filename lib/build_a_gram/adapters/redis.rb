require 'redis'

require_relative 'base'

module BuildAGram
  module Adapters
    class Redis < Base
      PopTimeout = 2

      attr_reader :host, :port, :timeout, :dumper

      def initialize(host: nil,
                     port: nil,
                     timeout: PopTimeout,
                     dumper: Marshal,
                     **options)
        super(**options)
        @host    = host
        @port    = port
        @timeout = timeout
        @dumper  = dumper
      end

      def push(message)
        redis.lpush(queue_key, dumper.dump(message))
      end

      def pop
        response = redis.brpop(queue_key, timeout: timeout) until response

        _, dumped_message = response

        dumper.load(dumped_message)
      end

      def redis
        @redis ||= ::Redis.new(host: host, port: port)
      end
    end
  end
end
