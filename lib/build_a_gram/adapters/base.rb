require 'logger'

module BuildAGram
  module Adapters
    class Base
      attr_reader :queue_key, :logger

      def initialize(queue_key:,
                     logger: Logger.new(STDOUT),
                     **options)
        @queue_key = queue_key
        @logger    = logger
      end

      def push(message)
        fail NotImplementedError
      end

      def pop
        fail NotImplementedError
      end
    end
  end
end
