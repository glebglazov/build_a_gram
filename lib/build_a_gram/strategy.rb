responders_dir = File.expand_path('../responders/**/*.rb',__FILE__)

Dir.glob(responders_dir).each do |file_path|
  require file_path
end

module BuildAGram
  class Strategy
    WrongUsageError = Class.new(StandardError) do
      def message
        "You've provided both handler and block: choose one"
      end
    end

    def self.builder(&block)
      proc do |api|
        s = new(api)
        yield s
        s
      end
    end

    attr_reader :api

    def initialize(api)
      @api        = api
      @responders = []
    end

    def call(message)
      @responders.each { |responder| responder.(message) }
    end

    def method_missing(type, *args, &block)
      register(Responders[type], *args, &block)
    end

    private

    def register(responder_klass, *args, handler: nil, **options, &block)
      fail WrongUsageError if handler && block

      block = proc { |*args| handler.(*args) } if handler

      @responders << responder_klass.new(self, *args, **options, &block)
    end
  end
end
