require_relative 'base_responder'

module BuildAGram
  module Responders
    class TextResponder < BaseResponder
      attr_reader :regexp

      def initialize(*, regexp:)
        super
        @regexp = regexp
      end

      def call(message)
        if matcher = regexp.match(message.text)
          behavior.(api, message, matcher)
        end
      end

      Responders.register(self)
    end
  end
end
