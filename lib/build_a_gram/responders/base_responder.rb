module BuildAGram
  module Responders
    ResponderNotFoundError = Class.new(StandardError)

    @@mapping = {}

    class << self
      def [](type)
        responder_klass = mapping[type]

        fail ResponderNotFoundError unless responder_klass

        responder_klass
      end

      def mapping
        @@mapping
      end

      def register(klass, as: nil)
        as ||= klass.name.split('::').last.sub(/Responder\z/, '').downcase

        @@mapping["on_#{as}".to_sym] = klass
      end
    end

    class BaseResponder
      attr_reader :strategy, :behavior

      def initialize(strategy, **, &block)
        @strategy = strategy
        @behavior = block
      end

      private

      def api
        @api ||= strategy.api
      end
    end
  end
end
