require_relative 'base_responder'

module BuildAGram
  module Responders
    class CommandResponder < BaseResponder
      attr_reader :name

      def initialize(*, name:)
        super
        @name = name
      end

      def call(message)
        if message.text =~ regexp
          behavior.(api, message)
        end
      end

      private

      def regexp
        @regexp ||= /\A\/#{name}(@#{me})?\Z/
      end

      def me
        @me ||= api.get_me.dig('result', 'username')
      end

      Responders.register(self)
    end
  end
end
