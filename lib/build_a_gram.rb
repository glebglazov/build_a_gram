require 'logger'
require 'telegram/bot'
require 'build_a_gram/strategy'

module BuildAGram
  class << self
    def logger
      @logger ||= Logger.new(STDOUT, level: ENV['LOGGER_LEVEL'] || Logger::INFO)
    end

    def Strategy(&block)
      BuildAGram::Strategy.builder(&block)
    end
  end
end

require 'build_a_gram/workers/messages_handlers/poller'
require 'build_a_gram/workers/messages_handlers/processor'
require 'build_a_gram/strategy'
